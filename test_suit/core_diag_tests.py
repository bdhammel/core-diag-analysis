import os, sys

TEST_DIR = os.path.dirname(os.path.abspath(__file__))
CORE_DIR = os.path.dirname(TEST_DIR)

if CORE_DIR not in sys.path:
    sys.path.append(CORE_DIR)

from core_diag import *
import unittest
from numpy import testing as nptest

print TEST_DIR
# Path to test data
PATH = os.path.join(TEST_DIR, 'test_data/')


#----------------------------------------------------------------------#
#   Testing the functionality of the Scope Class
#----------------------------------------------------------------------#
#
class ScopeTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.scopes = import_all_scope_preferences(path_to_json_file='test_data/scopes.json')

        cls.initial_time = np.arange(0.,105)*10**(-9)
        raw_data = {'Time (sec)':cls.initial_time,
                    'Ch. 1':np.arange(0,10),
                    'Ch. 2':np.arange(0,10),
                    'Ch. 3':np.arange(0,10),
                    'Ch. 4':np.arange(0,10)
                    }
        cls.delays = {"Ch. 1":13, "Ch. 2":70.5, "Ch. 3":35, "Ch. 4":107}
        cls.names = {"Ch. 1":'Ch. 1', "Ch. 2":'Ch. 2', "Ch. 3":'Ch. 3', "Ch. 4":'Ch. 4'}
        cls.scope = import_scope_preferences(
                                    'TLS1_ZEBRA', 
                                    path_to_json_file='test_data/scopes.json')
        cls.scope.set_delays(cls.delays)
        cls.scope.load_data(raw_data)
        cls.expected_t0_data = {'Ch. 1':np.arange(-13.,92),
                                'Ch. 2':np.arange(-70.5,34),
                                'Ch. 3':np.arange(-35.,70),
                                'Ch. 4':np.arange(-107.,-2)
                                }

        # disable printing to console
        sys.stdout = open(os.devnull, "w")

    # check that json format is being converted to Python objects 
    def test_json_to_python(self):

        for scope_name in self.scopes.iterkeys():
            self.assertIsInstance(self.scopes[scope_name], Scope)

    # assert the specific scopes can be retrieved
    def test_load_specific_scope(self):
        scope_name = 'TLS1_ZEBRA'
        scope = import_scope_preferences(scope_name, 'test_data/scopes.json')

        # confirm that the only scope is the desired one
        self.assertEqual(scope_name, scope.full_name)

    # is a ValueError thrown if user tries to load a scope form json that doesn't
    # exist 
    def test_no_json_scope(self):
        scope_name = 'TLS0_NONE'

        with self.assertRaises(ValueError):
            import_scope_preferences(scope_name, 'test_data/scopes.json')

    # confirm all channels are being loaded into the python scope objects
    def test_chs_in_scopes(self):
        self.assertEqual(self.scopes['TLS1_ZEBRA'].number_of_channels, 4)
        self.assertEqual(self.scopes['TLS2_ZEBRA'].number_of_channels, 16)

    # should return raw data in the correct format {"Ch 1.":{"Name":..., "Time":..
    def test_raw_data_from_scope(self):
        channels = self.scope.get_channels()
        for channel_name, channel_data in channels.iteritems():
            self.assertIn(channel_data.number, self.names.values())
            nptest.assert_array_almost_equal(channel_data.time(
                                                    units='s',
                                                    delay_corrected=False), 
                                             self.initial_time,
                                             1)
            nptest.assert_array_equal(channel_data.signal(), np.arange(0,10))


    # confirm correct removal of cable delays
    def test_cable_delay_correction(self):

        channels = self.scope.get_channels()

        for channel, channel_data in channels.iteritems():
            nptest.assert_array_almost_equal(channel_data.time(t0_corrected=False), 
                                             self.expected_t0_data[channel],
                                             10)
    
    # assert that the set_tzero function correctly sets the time zero to the trigger
    # channel
    @unittest.skip("need to figure out problem with rounding issue")
    def test_set_tzero(self):
        channels = self.scope.get_channels() 
        expected_values = {channel:self.initial_time-time
                            for channel, time in self.expected_t0_data.iteritems()}
        for channel, channel_data in channels.iteritems():
            nptest.assert_array_equal(channel_data["Time (ns)"], expected_values[channel])

    # Check that the parser for evaluating math expressions
    # is '1+1+ 1' = 3.0     is '1.5+1.5' = 3
    def test_math_parsing_of_json(self):
        delay_string =  {"Ch. 1":13, "Ch. 2":u"30+ 20 +15+5.5", 
                         "Ch. 3":u"20+15", "Ch. 4":u"107"}

        self.scope.set_delays(delay_string)

        for channel_name, channel in self.scope.get_channels().iteritems():
            self.assertEqual(self.delays[channel_name], channel.delay)

    # reenable printing to the console 
    @classmethod
    def tearDownClass(cls):
        sys.stdout = sys.__stdout__

#----------------------------------------------------------------------#
#   Test functionality for importing files
#----------------------------------------------------------------------#
#
class GetPathTests(unittest.TestCase):

    def test_clean_mac_path(self):
       """check that the path the user enters is returned in the required format for 
       a mac
       """
       path = '/Users/username/Documents/Folder\ With\ Spaces ' 
       clean_path = '/Users/username/Documents/Folder With Spaces' 
       cleaned_path = prompt_for_path(path)
       self.assertEqual(clean_path, cleaned_path)

    # check that the path is cleaned correctly for using on windows
    @unittest.skip("Need to fix windows path")
    def test_clean_windows_path(self):
       path = '/Users/username/Documents/Folder\ With\ Spaces ' 
       clean_path = '/Users/username/Documents/Folder With Spaces' 
       cleaned_path = prompt_for_path(path)
       self.assertEqual(clean_path, cleaned_path)

#----------------------------------------------------------------------#
#   Test functionality for reading from files
#----------------------------------------------------------------------#
class CleanScopeTest(unittest.TestCase):

    # disable printing to the console
    @classmethod
    def setUpClass(cls):
        sys.stdout = open(os.devnull, "w")

    # assert that the scope file name can be properly digested - and returns in 
    # expected format
    def test_cleaning_of_scopename(self):
        scope_path = PATH+'Z_SHOT3296_TDS1_DIAG.XLS'

        expected_scopename = u'TDS1'
        expected_scopetype = u'DIAG'
        expected_shotnumber = u'SHOT3296'

        shotnumber, scopename, scopetype = clean_scope_file_name(scope_path)

        self.assertListEqual([expected_scopename, expected_scopetype, expected_shotnumber],
                             [scopename, scopetype, shotnumber])

    # assert that if a scope is missnamed, it will throw an error
    def test_missnamed_scope_fails(self):
        scope_path = PATH+'Z_SHOT3296_TDS1_DIAG_break.XLS'

        with self.assertRaises(ValueError):
            shotnumber, scopename, scopetype = clean_scope_file_name(scope_path)

    # check that the scopes, saved in the formate currently being used, can be
    # read in correctly
    def test_read_from_facility_scope(self):
        scope_path = PATH+'Z_SHOT3296_TDS1_DIAG.XLS'

        data = read_facility_scope(scope_path)

        expected_column = np.arange(0,6)
        expected_channels = [u"Time (sec)", u"Ch. 1", u"Ch. 2", u"Ch. 3", u"Ch. 4"]

        # check that all of the titles are returned
        self.assertItemsEqual(data.keys(), expected_channels)

        # confirm that a numpy array object is returned, and the column is what is
        # expected
        for column in data.values():
            self.assertIsInstance(column, np.ndarray)
            self.assertListEqual(list(column), list(expected_column))

    # Check that an old scope can be read in in the same format 
    def test_read_from_old_scope(self):
        scope_path = PATH+'Z_SHOT2901_TDS1_DIAG.XLS'

        data = read_old_scope(scope_path)

        expected_column = np.arange(0,6)
        expected_channels = [u"Time (sec)", u"Ch. 1", u"Ch. 2", u"Ch. 3", u"Ch. 4"]

        # check that all of the titles are returned
        self.assertItemsEqual(data.keys(), expected_channels)

        # confirm that a numpy array object is returned, and the column is what is
        # expected
        for column in data.values():
            self.assertIsInstance(column, np.ndarray)
            self.assertListEqual(list(column), list(expected_column))

    # make sure the script quits if
    def test_value_error_scope_not_found(self):

        with self.assertRaises(ValueError):
            read_scope_from_directory(PATH, name='TDS1', type_='NONE')

    # reenable printing to the console 
    @classmethod
    def tearDownClass(cls):
        sys.stdout = sys.__stdout__

#----------------------------------------------------------------------#
#   Check some of the numerical methods used 
#----------------------------------------------------------------------#
class NumericalMethodTests(unittest.TestCase):

    # test the quality of the integration method 
    def test_integration_method(self):

        time = np.linspace(0,10*np.pi,10000)
        initial_signal = np.cos(time)
        expected_signal = np.sin(time)

        _, signal = integrate_signal(time, initial_signal)

        nptest.assert_array_almost_equal(signal, expected_signal[:-1], 2)

#----------------------------------------------------------------------#
#   run tests
#----------------------------------------------------------------------#
if __name__ == '__main__':
    unittest.main()
