import wx
import os
import json

CORE_DIR = os.path.dirname(os.path.abspath(__file__+'/..'))

"""
Must be run with framework python
"""

class Channel(object):
    def __init__(self, channel_number, verbose_name, delay, attenuation): 
        self.number = channel_number.split()[-1]
        self.verbose_name = verbose_name
        self.delay = str(delay)
        self.attenuation = str(attenuation)

    def as_dict(self):
        return {"verbose_name":self.verbose_name, "delay":self.delay,
                "attenuation":self.attenuation}

class Scope(object):
    def __init__(self, scopename, scopetype, trigger, channels): 
        self.scopename = scopename
        self.scopetype = scopetype
        self.trigger = trigger
        self.load_channels(channels)

    @property
    def full_name(self):
        return '_'.join([self.scopename, self.scopetype])
    
    def load_channels(self, channels):

        self.channels = {}

        for channel_number, channel in channels.iteritems():
            self.channels[channel_number] = Channel(
                                                channel_number=channel_number,
                                                **channel
                                                )
    def as_dict(self):
        dict_channels = {}

        for channel_name, channel in self.channels.iteritems():
            dict_channels[channel_name] = channel.as_dict()

        return {
                    "scopename":self.scopename, 
                    "scopetype":self.scopetype, 
                    "trigger":self.trigger,
                    "channels":dict_channels
                    }
                                                

class ScopeFrame(wx.Frame):
    _frame_size = (-1, -1)

    def __init__(self, scope, *args, **kwargs):
        super(ScopeFrame, self).__init__(*args, **kwargs)
        self.scope = scope

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)

        self.initFrame()

        # Set properties of the frame
        #self.SetSize(self._frame_size)
        self.SetTitle(scope.full_name)

        # Set up Done button to close the program 
        close_btn = wx.Button(self, label='Done')
        close_btn.Bind(wx.EVT_BUTTON, self._on_close)
        self.sizer.Add(close_btn, flag=wx.ALL, border=5)
        self.Fit()

        self.Show(True)

    def initFrame(self):
        name =  wx.StaticText(self,
                label="Name: " + self.scope.scopename)
        type =  wx.StaticText(self,
                label="Type: " + self.scope.scopetype)
        self.sizer.Add(name, flag=wx.ALL, border=5) 
        self.sizer.Add(type, flag=wx.ALL, border=5)  
        self.sizer.AddSpacer(10)

        trig_sizer = wx.BoxSizer(wx.HORIZONTAL)
        trigger_txt =  wx.StaticText(self,
                label="Trigger on: ")
        self.trigger_box = wx.TextCtrl(self, 
                    value=self.scope.trigger)
        trig_sizer.Add(trigger_txt)
        trig_sizer.Add(self.trigger_box)
        self.sizer.Add(trig_sizer, flag=wx.ALL, border=5)

        self.channel_input = {}

        ch_sizer = wx.BoxSizer(wx.HORIZONTAL)
        title =  wx.StaticText(self,
                    label="Ch #" +" "*10+"Verbose Name"+" "*20+"Delay"+" "*15+"Attenuation")
        ch_sizer.Add(title, flag=wx.ALL, border=5) 
        self.sizer.Add(ch_sizer, flag=wx.ALL, border=2)
        self.sizer.Add(wx.StaticLine(self, -1,  size=(479,2), style=wx.LI_HORIZONTAL))

        for channel_number in range(1,len(self.scope.channels)+1):
            channel_name = "Ch. {}".format(channel_number)
            channel = self.scope.channels[channel_name]
            ch_sizer = wx.BoxSizer(wx.HORIZONTAL)
            number =  wx.StaticText(self,
                        label=channel_name)
            verbose_name = wx.TextCtrl(self, 
                        value=channel.verbose_name,
                        size = (150, -1))
            delays = wx.TextCtrl(self, 
                        value=channel.delay,
                        size = (100, -1))
            attenuation = wx.TextCtrl(self, 
                        value=channel.attenuation,
                        size = (100, -1))
            ch_sizer.Add(number, flag=wx.ALL, border=5) 
            ch_sizer.Add(verbose_name, flag=wx.ALL, border=5) 
            ch_sizer.Add(delays, flag=wx.ALL, border=5) 
            ch_sizer.Add(attenuation, flag=wx.ALL, border=5) 
            
            self.channel_input[channel_name] = {
                                    "verbose_name":verbose_name,
                                    "delays":delays,
                                    "attenuation":attenuation
                                    }

            self.sizer.Add(ch_sizer, flag=wx.ALL, border=5)

    def _on_close(self, event):
        """Close the program and print values to screen

        called when done button is clicked
        """
        for channel_name, channel_boxes in self.channel_input.iteritems():
            channel = self.scope.channels[channel_name]
            channel.verbose_name = channel_boxes["verbose_name"].GetValue()
            channel.delay = channel_boxes["delays"].GetValue()
            channel.attenuation = channel_boxes["attenuation"].GetValue()

        self.scope.trigger = self.trigger_box.GetValue()
        self.Close()

class AllScopeFrame(wx.Frame):

    _frame_size = (200, 500)

    def __init__(self, *args, **kwargs):
        super(AllScopeFrame, self).__init__(*args, **kwargs)

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)

        self._import_param_data()
        self._load_buttons()

        # Set properties of the frame
        self.SetSize(self._frame_size)
        self.SetTitle('Scopes')

        close_btn = wx.Button(self, label='Save')
        close_btn.Bind(wx.EVT_BUTTON, self._on_close)
        self.sizer.Add(close_btn, flag=wx.ALL, border=5)

        self.Fit()
        self.Show(True)

    def _import_param_data(self, json_scopes=CORE_DIR+'/timing/cable_delays.json'):

        self.cable_delay_file = json_scopes

        with open(self.cable_delay_file, 'r') as f:
            json_scopes = json.load(f)

        self.scopes = []
        for scope in json_scopes:
            self.scopes.append(Scope(**scope))

    def _load_buttons(self):
        buttons = {}
        for scope in self.scopes:
            button = wx.Button(self, -1, scope.full_name)
            button.Bind(wx.EVT_BUTTON, self._open_scope) 
            button.scope_object = scope
            buttons[scope.full_name] = button
            #scope.link_button(button)
            self.sizer.Add(button, proportion=0, flag=wx.ALL, border=5)

    def _open_scope(self, event):
        scope = event.GetEventObject().scope_object
        scope_frame = ScopeFrame(scope, parent=None)

    def _on_close(self, event):
        """Close the program and print values to screen

        called when done button is clicked
        """
        with open(self.cable_delay_file, 'w+') as f:
            json.dump(
                [scope.as_dict() for scope in self.scopes], 
                f,
                sort_keys=True,
                indent=4)
        self.Close()

if __name__ == '__main__':
    app = wx.App()
    frame = AllScopeFrame(parent=None)
    app.MainLoop()
