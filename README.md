# Analysis Routines for Zebra Diagnostics 

This folder contains analysis scripts for diagnostics on the zebra machine.

## Scrips
 - `raw_scope_data.py`
 - `anode_bdot.py`
	Quick routine for analyzing the bdots on the anode plate. Calibration factors are set inside the `anode_bdot.py` file, timing is set in the `timing/cable_delays.json` file
 - `xray_diag.py`


## Installation

###Download [Homebrew](http://brew.sh/)

In `terminal` (press `CMD` + `space`, type "terminal", press `enter`)


copy & paste:
~~~bash
$ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
~~~

confirm that installation worked

~~~bash
$ brew update
$ brew doctor
~~~

If `brew doctor` returns an error, follow the suggestions until you get 

~~~bash
Your system is ready to brew.
~~~

### Downloading Python - For Mac
Type:

~~~bash
$ python --version
Python 2.7.5
~~~

If you have a version < 2.7 you'll need to upgrade via homebrew:

~~~bash
$ brew install python
~~~

### Install Dependencies

Use `brew` to install gcc and freetype

~~~bash
$ brew install freetype
$ brew install gcc
~~~

### Install PIP

~~~bash
$ easy_install pip
~~~

### Setup a [virtual environment](http://docs.python-guide.org/en/latest/dev/virtualenvs/)

#### Install virtualenv

~~~bash
$ pip install virtualenv
~~~

#### Create a new environment

cd into the core_diag directory and setup environment 


~~~bash
$ cd path/to/core_diag
$ virtualenv venv
~~~

#### Activate new environment 
~~~bash
$ source venv/bin/activate
(venv)$
~~~

### Install core_diag requirements

~~~bash
(venv)$ pip install -r ./requirements.txt
~~~


## Running Analysis Scripts

launch ipython

~~~bash
(venv)$ ipython
~~~

run a file

~~~ipython
In [1]: %run anode_bdot.py
    #======================================================================#
    #   B-Dot Analysis
    #======================================================================#
    
       to save the trace: shotXXXX = current_trace
       to display a saved trace onto the current plot: append_line(trace)
       If line is inverted: run core_diag.py and call "invert()"
    

Drag and drop full directory into command prompt
~~~

## Examples

See examples [here](www.thebrokendesk.com)
