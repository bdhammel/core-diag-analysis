#==============================================================================#
#   Common methods used for all core diagnostic analysis
#==============================================================================#
#
#   Author: Ben Hammel
#
#   This is imported into all the modules in the core_diag directory
#
#   The functions included in this file are used in all core diagnostic analysis
#   files. 
#
#    - Directory Processing: handles cleaning and data retrieval from shot files
#    - Graph Manipulations: Commonly used actions for adjusting traces on graphs
#    - Numerical Methods: Commonly used algorithms for signal analysis
#

import glob
import os.path
import sys
import xlrd
import json
import csv
import numpy as np
from scipy.optimize import minimize

CORE_DIR = os.path.dirname(os.path.abspath(__file__+'/..'))

if CORE_DIR  not in sys.path:
    sys.path.append(CORE_DIR)

from core import cmath

#==============================================================================#
#   Scope Objects
#==============================================================================#

class Channel(object):
    """Holds parameters for scope channels
    """

    def __init__(self, number, verbose_name=None, delay=0, attenuation=0):
        self.number = number
        self.verbose_name = verbose_name
        self.set_delay(delay)
        self.set_attenuation(attenuation)

    @property
    def name(self):
        return "Ch. " + self.number

    def _parse_math(self, string):
        """
        Splits string based on + operators and preforms addition on the numbers.
        other operators (- * /) do not work
        """
        if type(string) == unicode:
            if len(string) == 0:
                delay = 0
            else:
                delay = np.array(map(float, string.split('+'))).sum()
        else:
            delay = string 

        return delay

    def _signal_multiplier(self):
        """Calculate the multiplier for the signal base on attenuation in the
        cables

        TODO
        ----
        Write Test
        """
        return np.exp(-1*self.attenuation/20.)

    def set_delay(self, delay_string):
        """Parse delay string from json file into float

        Parameters
        ----------
        delay_string : str
            '' | '30' | '30 + 50+22'
        """
        
        self._delay_string = delay_string
        self.delay = self._parse_math(delay_string)

    def set_attenuation(self, attenuation_string):
        """Parse attenuation string from json file into float

        Parameters
        ----------
        attenuation_string : str
            '' | '30' | '30 + 50+22'
        """
        self._atten_string = attenuation_string
        self.attenuation = self._parse_math(attenuation_string)


    def additional_delay(self, delay):
        """Add an additional delay to channel delay
        typically time of flight or internal delay for a detector

        Parameters
        ----------
        delay : float
            delay to be added to channel in ns
        """
        self.delay += delay

    def load_data(self, time, data):
        """load the data into the channel
        """
        self._time = np.array(map(float, time))
        self._signal = np.array(map(float, data))

    def load_t0_correction(self, trigger_channel):
        """Load in the delay necessary to triggering the scope
        """
        self.delay_from_trigger = trigger_channel.delay
        self.t0_ref_channel = trigger_channel.number

    def set_faux_trigger(self, trigger, trigger_level):
        """Load a fake triggering channel

        Parameters
        ----------
        trigger : channel
            Channel object that to set the new trigger off of
        trigger_level : float
            the level at which the channel should trigger
        """
        i = np.where(trigger.signal() > 1.)[0][0]
        trigger_delay = trigger.time()[i]
        print("offsetting traces by {}".format(trigger_delay))
        self.delay_from_trigger -= trigger_delay
        self.t0_ref_channel = trigger.number

    def time(self, units='ns', t0_corrected=True, delay_corrected=True):
        """Convert time into desired units.

        Assumes time is stored in seconds

        Parametes
        ---------
        time_units : str 
            ns | us | ms | s
        t0_corrected : bool
            Compensate for cable delays and trigger delays
        delay_corrected : boob
            Compensate for cable delays
        """
        conversion_factor = {'ns':10**9, 'us':10**6, 'ms':10**3, 's':1}
        
        time = self._time * conversion_factor["ns"]

        if t0_corrected:
            time = time - self.delay + self.delay_from_trigger
        elif not t0_corrected and delay_corrected:
            time = time - self.delay

        if units != 'ns':
            time *= (conversion_factor[units] / conversion_factor["ns"])

        return time

    def signal(self, normalize=False):
        """
        """
        if normalize:
            if self._signal.max() > (-1*self._signal.min()):
                signal = self._signal/self._signal.max()
            else:
                signal = -1*self._signal/self._signal.min()
        else:
            signal = self._signal

        return signal*self._signal_multiplier()

    def data(self, as_dict=False):
        
        time = self.time()
        signal = self.signal()

        if as_dict:
            return {"time":time, "signal":signal}
        else:
            return time, signal

    def prefrences_as_dict(self):
        pass

class Scope(object):
    """ Holder for information related to a facility scope

    Parameters
    ----------
    scopename : str 
        The TLS or TDS representation of a facility scope
    scopetype : str 
        The ZEBRA or DIAG representation of a facility scope
    trigger : str 
        Channel responsible for triggering the scope

    """

    def __init__(self, scopename, scopetype, trigger):
        self.name = scopename
        self.type = scopetype
        self.trigger_channel = trigger
        self.channels = {} 

    # return NAME_TYPE
    @property
    def full_name(self):
        return self.name + '_' + self.type

    # return the numbertt of channels in the scope
    @property
    def number_of_channels(self):
        return len(self.channels)

    # Add a channel object to the scope 
    def add_channel(self, channel):
        self.channels[channel.number] = channel

    def load_data(self, d):
        """Load data read in from facility scopes

        Parameters
        ----------
        d : dic 
            {'Time (sec)':[], 'Ch. 1':[], ...}
        """
        if len(d)-1 != len(self.channels):
            print("Expected {} channels. Only got {}".format(
                                                        len(self.channels), 
                                                        len(d)-1)
                                                        )
        trigger_ch = self.channels[self.trigger_channel]

        for key in d:
            if key != 'Time (sec)': 
                self.channels[key].load_data(d['Time (sec)'], d[key])
                self.channels[key].load_t0_correction(trigger_ch)

    def set_delays(self, delays):
        for channel_name, channel in self.channels.iteritems(): 
            channel.set_delay(delays[channel_name])

    def get_channel(self, channel_name):
        """Correct all channel times for a common T_zero

        Parameters
        ----------
        channel_name : str 
            Of the form 'Ch. #' or 'all' to return all channels
        """
        return self.channels[channel_name]

    def get_channels(self, channel_names=['all']):
        """Returns all channels in a scope

        Parameters
        ----------
        channel_names : array
            Names of the channels to be returned or 'all'

        Returns
        -------
        channels : dict, {channel #:Channel}) 
            dictionary of channels
        """
        if 'all' in channel_names:
            return self.channels
        else:
            return [channel for channel in self.channels if channel.name in channel_names]
    
    def set_trigger_ch(self, trigger, trigger_level):
        """Move the channel triggered by the scope to a different channel
        """
        for ch in self.get_channels().values():
            ch.set_faux_trigger(trigger, trigger_level)

    def get_trigger_ch(self):
        """Return the channel used as a trigger
        """
        return self.get_channel(self.trigger_channel)


    def set_trigger_to_external_scope(self, external_scope):
        """Match the trigger used on an external scope

        If two scopes use the same signal as a trigger, the trigger channels 
        can be over lapped to improve accuracy of tzero. Find difference in 
        triggering time by matching the triggering signals for the two scoples. 
        Apply the difference in delay to each channel on this scope.

        Parameters
        ----------
        external_scope : Scope
            external scope to match triggering to

        TODO
        ----
        write Test

        """
        etrig = external_scope.get_trigger_ch()
        itrig = self.get_trigger_ch()

        etrigf = cmath.savitzky_golay(etrig.signal(normalize=True), 41, 4)
        itrigf = cmath.savitzky_golay(itrig.signal(normalize=True), 41, 4)

        j = 0
        res = minimize(cmath.err, j, args=(etrigf, itrigf), 
                method=cmath.minimize_by_index, options=dict(stepsize=1))

        j = res.x
        i = int(abs(j))

        if j < 0:
            delta = itrig.time()[0] - etrig.time()[i]
        else:
            delta = itrig.time()[i] - etrig.time()[0] 

        for ch in self.get_channels().itervalues():
            ch.additional_delay(delta)

        print "scope {scopename} trigger shifted by {val}".format(
                                                    scopename=self.full_name,
                                                    val=delta)



def import_all_scope_preferences(path_to_json_file=CORE_DIR+'/timing/cable_delays.json'):
    """Import all scopes in a json file

    Pull scope information form a json file and load it into Scope Objects

    Parameters
    ----------
    path_to_json_file : str, optional
        path to the file of scope information 

    Returns
    -------
    dict
        dictionary of Scope objects. {'NAME_TYPE': scope_object, ...}
    """

    with open(path_to_json_file) as cable_delay_file:
        json_scopes = json.load(cable_delay_file)

    scopes = {}

    for json_scope in json_scopes:
        scope = Scope(
                    scopename=json_scope['scopename'],
                    scopetype=json_scope['scopetype'],
                    trigger=json_scope['trigger'],
                    )
        for channel_number, json_channel in json_scope['channels'].iteritems():
            scope.add_channel(
                            Channel(
                                number=channel_number,
                                verbose_name=json_channel['verbose_name'],
                                delay=json_channel['delay'],
                                attenuation=json_channel['attenuation']
                                )
                            )
        scopes[scope.full_name] = scope

    return scopes

def import_scope_preferences(scope_name, path_to_json_file=CORE_DIR+'/timing/cable_delays.json'):
    """Import the cable delay json files, and import them into Scope objects

    Loads all of the scopes in the file, and iterates through them, selecting
    the scope the user asked for

    Parameters
    ----------
    scope_name : str
        the name of the scope in the format NAME_TYPE
    path_to_json_file : str, optional
        path to the file of scope information

    Returns 
    -------
    Scope Object
        scope object for the requested scope name
        
    """
    all_scopes = import_all_scope_preferences(path_to_json_file)

    for name, scope in all_scopes.iteritems(): 
        if name == scope_name:
            return scope

    raise ValueError("Preferences for scope {} weren't found".format(scope_name))

            
def get_scope(path, scopename, scopetype):
    """Return all data from scope file as python scope object

    Import preferences from timing file and load raw data from excel file in shot
    directory

    Parameters
    ----------
    path : str
        os path to shot directory
    scomename
    scopetype

    Returns
    --------
    scope : Scope
        python scope object 
    shotnumber : int
    """
    scope = import_scope_preferences(scopename + "_" + scopetype)
    shotnumber, raw_data = read_scope_from_directory(path, scopename, scopetype)
    scope.load_data(raw_data)

    return shotnumber, scope

#==============================================================================#
#   Directory Processing
#==============================================================================#

def prompt_for_path(path=None):
    """Ask user to submit path to the shot directory


    Prompts user to drag and drop shot directory into the terminal window, this
    path is 'cleaned' and returned so that the desired scopes inside the
    directory can be found

    Args:
        path (str, optional): The path can be submitted via the function call, 
                and the user wont be prompted through the terminal 

    Returns (str):
        path the user submitted, with the correct use of separators, and any
        leading, or trailing, spaces removed. 
    """

    if not path:
        print "Drag and drop location into command prompt"
        path = raw_input("path > ")

    print '\n',  # new line for spacing

    if os.name is 'posix':
        return path.strip().replace('\\', '')
    else:
        return path #.strip().replace('//', '')


def clean_scope_file_name(scope_path):
    """Extract the scope's name, the shot number, and the scope type

    Z_SHOT1234_TLS_ZEBRA -> returns:
        shotnumber: SHOT1234
        scopename:  TLS
        scopetype:  ZEBRA

    Args:
        scope_path (str): file name with the format 
            <path/to/file/Z_SHOTXXX_NAME_TYPE>

    Returns:
        shotnumber (str): The shot number of the scope being read "SHOTXXXX"
        scopename (str): TLS or TDS
        scopetype (str): ZEBRA or DIAG

    Raises:
        ValueError: if there is a scope in the directory that has a name other 
            than Z_SHOTXXX_NAME_TYPE

    """

    scope = os.path.basename(scope_path)

    try:
        shotnumber, scopename, scopetype = scope.split('_')[1:]
    except ValueError:
        error = '''
             The scope {} is named incorrectly inside this directory.
             All scopes must be of the form Z_SHOTXXX_NAME_TYPE
            '''.format(scope)
        print error
        raise

    return shotnumber, scopename, scopetype.split('.')[0]


def read_facility_scope(scope_path):
    """Read in the data from scope of interest

    Read in the data from an excel file created by the facility scopes
    This method assumes that the first sheet of the excel work-book is the data
    and that the first line is the titles of the columns. If the data is from an 
    old scope, read in data via read_old_scope()

    Args:
        scope_path (str): file name with the format 
            <path/to/file/Z_SHOTXXX_NAME_TYPE>

    Returns
        d (dict): dictionary of arrays for each column in the scope 
            {'Time (sec)':[], 'Ch. 1':[], ...}
    """
    titles = []
    data = []
    

    # Currently, Facility scopes are saved in a multi-page excel format
    # use xlrd to open the workbook
    # Old scopes will fail if this is tired. If XLRDError, the scope is 
    # in an old format - open in csv format
    try:
        # Open up excel document and access the first sheet
        wb = xlrd.open_workbook(scope_path)
        sh = wb.sheet_by_index(0)
    except xlrd.XLRDError:
        print "Excel failed, using CSV format"
        d = read_old_scope(scope_path)
    else:
        for row in range(sh.nrows):
            #   store titles from fist line
            if row == 0:
                for title in sh.row_values(row):
                    titles.append(title.strip())   
                    #   create a place for the data to be stored
                    data.append([])
            #   store data
            else:
                for x, value in enumerate(sh.row_values(row)):
                    try:
                        data[x].append(float(value))
                    except None:
                        pass

        #   generate a dictionary containing all of the data with data column titles
        #   as the dictionary keys
        d = {}
        for x, title in enumerate(titles):
            d[title] = np.array(data[x])
 
        print 'loading channels:' + '.'*10 + ' '.join(titles)

    return d


def read_old_scope(scope_path):
    """Read in a scope file from older format
    
    Old formats are a CSV file, and the time column is titles 'Time Sec'

    Args:
        scope_path (str): file name with the format 
            <path/to/file/Z_SHOTXXX_NAME_TYPE>

    Returns
        d (dict): dictionary of arrays for each column in the scope 
            {'Time (sec)':[], 'Ch. 1':[], ...} 
            (same format as read_facility_scope())

    """
    titles = []
    data = []

    with open (scope_path, 'U') as csvfile:
        reader = csv.reader(csvfile, delimiter = '\t' )
    
        for row in reader:
            # store titles from fist line
            if reader.line_num == 1:
                for ch, title in enumerate(row):

                    # correct for different time name
                    if title.strip() == "Time Sec":
                        print "switch"
                        title = "Time (sec)"
                    else:
                        title = "Ch. %s" %ch

                    titles.append(title.strip())   
                    # create a place for the data to be stored
                    data.append([])
            # store data
            else:
                for x, value in enumerate(row):
                    try:
                        data[x].append(float(value))
                    except:
                        pass

    # generate a dictionary containing all of the data with data column titles
    # as the dictionary keys
    d = {}
    for x, title in enumerate(titles):
        d[title] = np.array(data[x])

    print '\nloading channels:' + '.'*10 + ' '.join(titles)
    return d

def read_scope_from_directory(path, name, type_):
    """Searchers for the needed scope in the shot directory 

    shotnumber, d = read_scope_from_directory(path, name, type)

    Args:
        path (str): absolute path to the Z_SHOTXXX directory
        name (str): name of the scope i.e. TLS or TDS
        type_ (str): type of scope i.e. ZEBRA or DIAG
    
    Returns 
        shotnumber (str): SHOTXXXX
        d = {"Ch. 1": [], ...}
    """

    # Iterate through each file in the directory
    # get the scope name, type, and the shot number from clean_scope_file
    for scope_path in glob.iglob(path+'/*.XLS'):

        shotnumber, scopename, scopetype = clean_scope_file_name(scope_path)

        if (scopename == name) and (scopetype.split('.')[0]) == type_:
            d = read_facility_scope(scope_path)
            return shotnumber, d

    print("reading scope {name}_{type}".format(name=name, type=type_))

    # If the scope cannot be found in the directory, exit the program
    raise ValueError("The scope, {name}_{type}, was not found".format(name=name, type=type_))


#==============================================================================#
#
#==============================================================================#

def clean_variables(_name):
    """Delete all user defined variables in a session 

    Args:
     _name: __name__ variable of current working directory
    """
    _this = sys.modules[_name]

    for n in dir():
        if n[0]!='_': 
            delattr(_this, n)










