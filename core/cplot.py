import os
import sys
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np

CORE_DIR = os.path.dirname(os.path.abspath(__file__+'/..'))

if CORE_DIR  not in sys.path:
    sys.path.append(CORE_DIR)

from core import cmath

class ImageData(object):
    """Holder class for spacial image data

    Attributes:
        data (numpy array): 2d array
        xpixels (int): number of pixels in the x (time) direction 
        ypixels (int): number of pixels in the y (spacial) direction 
        xmax (float): maximum time on the trace
        ymax (float): maximum spacial point on the trace
    """

    def __init__(self, img, xmin, xmax, ymin, ymax):
        """
        Args:
            xmax (int): the length of the sweep window 
            ymax (int): the size of the slit on the streak camera as it 
                relates to the image focal size

        xmin
        xmax (float): maximum time on the trace
        ymin
        ymax (float): maximum spacial point on the trace
        """
        self._data = np.asarray(img)
        self.xpixels, self.ypixels = self._data.shape

        self.xmin = float(np.round(xmin, -1))
        self.xmax = float(np.round(xmax, -1)) 
        self.ymin = float(np.round(ymin, 1))
        self.ymax = float(np.round(ymax, 1)) 

    @property
    def xpixel_ratio(self):
        """xpixel_ratio (float): (time length of the image)/(# of pixels)
        """
        return (self.xmax-self.xmin)/self.xpixels

    @property
    def ypixel_ratio(self):
        """ypixel_ratio (float): (spacial size of the image)/(# of pixels)
        """
        return (self.ymax-self.ymin)/self.ypixels

    @property
    def x_range(self):
        """x_range (array): time array for x labels 
        """
        dx = np.round((self.xmax-self.xmin)/10)
        return np.arange(round(self.xmin), round(self.xmax,-1), dx)

    @property
    def y_range(self):
        """y_range (array): spacial array for y labels 
        """
        dy = (self.ymax-self.ymin)/10
        return np.around(np.arange(self.ymin, self.ymax, dy),2)

    @property
    def ypix_range(self):
        """xpix_range (array): pixel location for the x_range labels

        Because pixels are only positive numbers, and the time sequence doesn't
        necessary start at zero. We construct the pix range from the dirence between
        the starting and ending times
        """
        return (self.y_range-self.y_range.min()) / self.ypixel_ratio

    @property
    def xpix_range(self):
        """ypix_range (array): pixel location for the y_range labels

        Because pixels are only positive numbers, and the time sequence doesn't
        necessary start at zero. We construct the pix range from the dirence between
        the starting and ending times
        """
        return (self.x_range-self.x_range.min()) / self.xpixel_ratio

    # returns image with orientation <Time, Spacial> (<x, y>)
    def as_xy(self):
        return np.transpose(self._data)

    # returns image with orientation <Spacial, Time> (<x, y>)
    def as_yx(self):
        return self._data

    # return the locations where the tick marks should be for the time axes
    def xtick_locations(self):
        return self.xpix_range

    # return the mapping from pixel location to time 
    def xtick_labels(self):
        return self.x_range

    # return the locations where the tick marks should be for the time axes
    def ytick_locations(self):
        return self.ypix_range

    # return the mapping from pixel location to time 
    def ytick_labels(self):
        return self.y_range

    def pixel_to_physical(self, px, py):
        """Convert a pixel location to physical location
        """
        return (np.round(px*self.xpixel_ratio+self.xmin, -1), 
                np.round(py*self.ypixel_ratio+self.ymin, 3))

    def show(self, title='streak', color='gray', frame=True, figsize=(6,4)):
        
        fig = plt.figure(title, dpi = 100, figsize=figsize)
        fig.set_tight_layout(True)
        ax = fig.add_subplot(111)
        implot = ax.imshow(self.as_xy(), aspect='auto')
        implot.set_cmap(color)
        plt.show()

        if frame:
            ax.set_xticks(self.xtick_locations())
            ax.set_xticklabels(self.xtick_labels())
            ax.set_xlabel("Time [ns]")
            ax.set_xlim(0,self.xpixels)

            ax.set_yticks(self.ytick_locations())
            ax.set_yticklabels(self.ytick_labels())
            ax.set_ylabel("X [mm]")
            ax.set_ylim(0,self.ypixels)
        else:
            ax.set_axis_off()
            #fig.tight_layout(pad=0)
        
        plt.draw()


class IndexSelector(object):
    """
    Return the index of the x value clicked on the array

    Only works with one line on the graph.

    Attributes:
        _ax (Axis): axis the plot is drawn on
        xdata (nparray): xvalue of the line on the graph
        _cursor_index (int): the index value the cursor was placed at
        _line : object drawn on plot after user click
        _color: color the line should be

    Example:
        line, = ax.plot(xs, ys, 'o', picker=5) # setting picker is important!
        sel = IndexSelector(ax, line=line)

    """
    def __init__(self, ax, line_name=None, line=None, color='g'):
        """
        Args:
            line_name (str): the name of the line on a plot to get the data of
        """
        self._ax = ax

        if line:
            self.xdata = line.get_xdata()
        elif line_name:
            self.xdata = get_line_from_plot(line_name).get_xdata()
        else:
            self.xdata = self._ax.lines[0].get_xdata()

        self._cursor_index = None
        self._line = None
        self._color = color

        self.cidclick = self._ax.figure.canvas.mpl_connect("pick_event", self._on_pick)
        self.cidkey = self._ax.figure.canvas.mpl_connect("key_press_event", self._key_press)
        print "index Selector"

    # When there is a mouse click, record the location of the click, and draw
    # a line. 
    def _on_pick(self, event):
        try:
            self._cursor_index = event.ind[0]
        except ValueError:
            pass
        else:
            print("click at {} value {}".format(
                                        self._cursor_index, 
                                        self.xdata[self._cursor_index]))
            self.draw(self.xdata[self._cursor_index], color=self._color)

    def _key_press(self, event):
        print(event.key)
        sys.stdout.flush()
        if event.key == 'return' or 'enter':
            self.disconnect()

    # remove the vertical line if it's there
    def remove_line(self):
        if self._line:
            self._line.remove()

    def draw(self, location, color='g'):
        """Draw a line on the graph

        This function is overwritten in some inherited objects, depending on the
        type of object to be drawn on screen
        """
        self.remove_line()
        self._line = self._ax.axvline(location, color=color)
        plt.draw()

    # remove event handling
    def disconnect(self):
        self._ax.figure.canvas.mpl_disconnect(self.cidclick)
        self._ax.figure.canvas.mpl_disconnect(self.cidkey)

    # return the index of the line
    def get_index(self):
        return self._cursor_index


class FixedWindowSelector(IndexSelector):
    """Expansion of Index selector
    Selects window of fixed size off the location of the first click
    """
    
    def draw(self, location, window_width=480, color='g'):
        """Draw a line on the graph

        This function is overwritten in some inherited objects, depending on the
        type of object to be drawn on screen
        """
        self.remove_line()
        self._line = self._ax.axvspan(
                                    location, 
                                    location+window_width, 
                                    color=color, 
                                    alpha=.3)
        plt.draw()

    # return the index of the line
    def get_index(self):
        start = self._cursor_index
        endval = self.xdata[start] + 480
        end = np.where(self.xdata==endval)[0][0]
        return start, end

    def get_xvals(self):
        i1, i2 = self.get_index()
        return self.xdata[i1], self.xdata[i2]


def load_image(path, xmin=0, xmax=480, ymin=0, ymax=1, transpose=False):
    """Load the Raw Data

    load a png image into a numpy array
    image must be uploaded such that fringes are running up and down

    Parameters
    ----------
    path : str
        cleaned path loaded from user

    Return
    ------
    ImageData Object

    """
    img_array = plt.imread(path)

    if transpose:
        img_array = img_array.transpose(), 

    img = ImageData(
                    img_array, 
                    xmin=xmin,
                    xmax=xmax,
                    ymin=ymin,
                    ymax=ymax
                    )

    aspect = img.xpixels/img.ypixels
    figsize = (6,6/aspect)
    img.show('Raw Data', figsize=figsize)

    return img




#==============================================================================#
#   Graphing Manipulations
#==============================================================================#

def append_line(line, ax=None):
    """Appends lines to the current plot

    Takes return from plt.plot (i.e. line = plt.plot), and appends it to the
    current graph on the current axis. If no current axis, make a new plot

    Args:
        *Args (Line): arbitrary number of Line objects
 
    """
    if not ax:
        if plt.gca():
            ax = plt.gca()
        else:
            fig = plt.figure()
            ax = fig.add_subplot(111)

    ax.plot(line.get_xdata(), 
            line.get_ydata(), 
            label=line.get_label(),
            color=line.get_color()
            )

    plt.legend()

    plt.draw()


def get_line_from_plot(label):
    """Return a line from a plot given the label 

    Finds the line on a plot given its label (in the legend), Alert the user if
    this can't be found

    Args:
        label (str): The name of a trace on the plot

    Returns:
        line (Line): Line object of the desired trace
    """
    fig = plt.gcf()

    for ax in fig.get_axes():
        for line in ax.get_lines():
            if line.get_label() == label:
                return line

    print label + ' was not found'


def pop(label, ax=None):
    """Remove line from the graph

    Args:
        label (str): Name of the trace to remove from the graph
        ax (Axes Object, optional): axis where the line is located
    """
    line = get_line_from_plot(label)
    if line:
        line.remove()
        plt.draw()
        plt.legend()
        return line

def invert(label):
    """Invert a line 

    Finds the most recent line on a plot. Removes it, inverts it, and
    re plots it 

    Args:
        label (str): Name of the trace to invert
    """
    line = get_line_from_plot(label)
    if line:
        line.set_ydata(-1 * line.get_ydata())
        plt.draw()

def align_yaxis(v1=0,v2=0):
    """Align the y axies for a twin x plot 

    Args:
        v1 (float, optional): The place on ax1 to align to, default is 0 
        V2 (float, optional): the place on ax2 to align to, default is 0 
    """

    fig = plt.gcf()
    try:
        ax1, ax2 = fig.axes
    except ValueError:
        print "Need to axes to align, only one! Doing nothing"
        return None

    #adjust ax2 ylimit so that v2 in ax2 is aligned to v1 in ax1
    _, y1 = ax1.transData.transform((0, v1))
    _, y2 = ax2.transData.transform((0, v2))
    inv = ax2.transData.inverted()
    _, dy = inv.transform((0, 0)) - inv.transform((0, y1-y2))
    miny, maxy = ax2.get_ylim()
    ax2.set_ylim(miny+dy, maxy+dy)
    plt.draw()

#----------------------------------------------------------------------#
#   integrate a trace on a plot
#----------------------------------------------------------------------#
def integrate_trace(label):
    line = get_line_from_plot(label)
    if line:
        fig = plt.gcf()
        axs = fig.axes
        ax1 = axs[0]
        signal = line.get_ydata()
        signal -= signal[:200].mean()  #ignore noise
        time = line.get_xdata()
        line.remove()
        plt.draw()
        ax1.autoscale()

        integrated_sig = cmath.integrate_signal(time, signal)
        if len(axs) == 1:
            ax2 = ax1.twinx()
        else:
            ax2 = axs[1]
        ax2.set_ylabel('int{Voltage (V)}')
        ax2.plot(time[:-1], 
                 integrated_sig, 
                 label=label+' (integrated)',
                 color=line.get_color())

        ax1.legend()
        ax2.legend(loc=4)
        align_yaxis()
        plt.draw()

def plot_on_differnt_y(ch, suppress_text=False):
    """Select a trace to plot on a separate Y axis
    
    Args:
        ch (str): the legend label for a trace on the graph
        suppress_text (boolean): if True, don't print out info for method
    """
    trace = pop(ch)

    ax1 = plt.gca()
    ax2 = ax1.twinx()
    append_line(trace, ax2)

    #align_yaxis()
    #ax2.plot(trace.get_xdata(), trace.get_ydata(), 'r')

    if not suppress_text:
        print "Trace" + ch + '''plotted on seperate Y. Origional axes (ax[0]) and 
        new axes (ax[1]) have been returned. Make sure to call align_yaxis() to
        recenter the traces'''

    return [ax1, ax2]


#----------------------------------------------------------------------#
#   smooth a trace
#----------------------------------------------------------------------#
def smooth_trace(label, window=31):
    line = get_line_from_plot(label)
    if line:
        data = line.get_ydata()
        smoothed_data = savitzky_golay(data, window_size=window, order=4)
        line.set_ydata(smoothed_data)
        plt.draw()


class DataCursor(object):
    text_template = 'x: %0.2f\ny: %0.3f'
    x, y = 0.0, 0.0
    xoffset, yoffset = -20, 20

    def __init__(self, ax):
        self.ax = ax
        self.annotation = ax.annotate(self.text_template, 
                xy=(self.x, self.y), xytext=(self.xoffset, self.yoffset), 
                textcoords='offset points', ha='right', va='bottom',
                bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
                arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0')
                )
        self.annotation.set_visible(False)

    def __call__(self, event):
        self.event = event
        # xdata, ydata = event.artist.get_data()
        # self.x, self.y = xdata[event.ind], ydata[event.ind]
        self.x, self.y = event.mouseevent.xdata, event.mouseevent.ydata
        if self.x is not None:
            self.annotation.xy = self.x, self.y
            self.annotation.set_text(self.text_template % (self.x, self.y))
            self.annotation.set_visible(True)
            event.canvas.draw()
