#==============================================================================#
#   Evaluate the current of one shot
#==============================================================================#
#   
#   script will prompt user to upload the path to a zebra shot.
#   Returns the current trace in MA and the T zero corrected time in nanoseconds
#

#----------------------------------------------------------------------#
#   Imports
#----------------------------------------------------------------------#
#   check path to see if the core_diag directory is in the system path
#   import the core_diag module for shared functionality between diagnostics
#       i.e. read and write files, graphing, etc

import os
import sys
import numpy as np
import matplotlib.pyplot as plt

CORE_DIR = os.path.dirname(os.path.abspath(__file__+'/..'))

if CORE_DIR not in sys.path:
    sys.path.append(CORE_DIR )

from core import cscopes
from core import cmath

#----------------------------------------------------------------------#
#   Cable delays
#----------------------------------------------------------------------#
#   Bdots used are ch 9 and ch 10 into TLS #2
#   vDot used is ch 13 on TLS #2

TLS2 = cscopes.import_scope_preferences('TLS2_ZEBRA')

#----------------------------------------------------------------------#
#   Calibration numbers and scaling factor for bdots
#----------------------------------------------------------------------#
'''
********************************************************************************
                            CHANGE THESE VALUES
********************************************************************************
'''
SCALING_FACTOR = 10**(3./2.)
BL1P_CALIBRATION = 1.35*10**11
BL1N_CALIBRATION = 1.35*10**11
P_CH = 'Ch. 9'
N_CH = 'Ch. 10'
'''
********************************************************************************
'''

#----------------------------------------------------------------------#
#   Get the two differential bdot signals 
#----------------------------------------------------------------------#
#
#   Returns two channels (specified by bdotp and bdotn) from the 
#   TLS2_ZEBRA scope, 
#
def get_bdot_signals(raw_data, bdotp_Ch=P_CH, bdotn_Ch=N_CH):
    """get the bdot signals from the scope, adjust to correct time
    
    Set a fake trigger on the scopes to but current at tzero
    """

    TLS2.load_data(raw_data)
    trigger = TLS2.get_channel(P_CH)
    TLS2.set_trigger_ch(trigger, trigger_level=1.)

    print "Using bdot signals %s and %s" %(P_CH,
                                           N_CH)

    bdotp_time = TLS2.get_channel(P_CH).time()
    bdotp      = TLS2.get_channel(P_CH).signal()
    bdotn_time = TLS2.get_channel(N_CH).time()
    bdotn      = TLS2.get_channel(N_CH).signal()

    '''
    a method should be added here to combined the two times
    '''

    return bdotp_time, bdotn, bdotp

#----------------------------------------------------------------------#
#   Find the current from time corrected scope. 
#----------------------------------------------------------------------#
#
#   Two signals from the anode plate bdot (ch 9 and ch 10 on TLS2)
#   at opposite polarities are averaged to cancel noise. The trace is integrated
#   and scaled to give the current
#   Time is converted from seconds to nanoseconds
#   
def current_from_bdot(raw_data):

    # get the time corrected bdot signals
    time, bdot_n, bdot_p = get_bdot_signals(raw_data)

    # apply the calibration / scaling factor to the trace
    bl1_p = bdot_n * SCALING_FACTOR * BL1P_CALIBRATION
    bl1_n = bdot_p * SCALING_FACTOR * BL1N_CALIBRATION 

    # ignore noise for the first 200 points
    bl1_p -= bl1_p[:200].mean()
    bl1_n -= bl1_n[:200].mean()

    #   average the differential p / n traces
    V = (bl1_p - bl1_n)/2.

    V -= V[:200].mean()
    t, I = cmath.integrate_signal(time, V)
    I = cmath.flip_if_inverted(I)

    #   convert to MA/ns
    I *= 10**(-6) *10**(-9)

    return t, np.array(I)

#----------------------------------------------------------------------#
#   graph the current
#----------------------------------------------------------------------#
def graph_current(shotnumber, time, current):
    ax = plt.gca()
    plot = ax.plot(time, current, label=shotnumber)
    plt.xlim(-100,300)
    plt.ylim(-.1,1)
    plt.ylabel("Current (MA)")
    plt.xlabel("Time (ns)")
    plt.legend()
    return plot[0]

#----------------------------------------------------------------------#
#   Prompt the user to load a file to be analysed
#----------------------------------------------------------------------#
#
#   Given a path to a shot directory, find the necisary scope, convert desired
#   bdot signals to current, plot, and return graph trace
#
def get_current(directory_path):
    shotnumber, d = cscopes.read_scope_from_directory(directory_path, 'TLS2', 'ZEBRA')
    time, current = current_from_bdot(d)
    trace = graph_current(shotnumber, time, current)
    return trace

#----------------------------------------------------------------------#
#   Main 
#----------------------------------------------------------------------#
#
#   Turn on interactive python so multiple graphs can be shown
#   prompt user for the shot directory
#
if __name__ is "__main__":
    plt.ion()
    print '''
    #======================================================================#
    #   B-Dot Analysis
    #======================================================================#
    
       to save the trace: shotXXXX = current_trace
       to display a saved trace onto the current plot: append_line(trace)
       If line is inverted: run core_diag.py and call "invert()"
    
    '''
    #plt.close('all')
    path = cscopes.prompt_for_path()
    current_trace = get_current(path)


