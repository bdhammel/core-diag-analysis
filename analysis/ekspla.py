import os.path
import sys
import matplotlib.pyplot as plt
import numpy as np

CORE_DIR = os.path.dirname(os.path.abspath(__file__+'/..'))

if CORE_DIR not in sys.path:
    sys.path.append(CORE_DIR )

from core import cscopes
from core import cmath

#----------------------------------------------------------------------#
#   import the ekspla data from TDS8_DIAG
#----------------------------------------------------------------------#
#
#   finds the maxumum of the ekspla light pulse from the photodiode going into
#   channel 8 on TDS8 in the shadowgraphy screenbox
#   Return the two locations in time based on the max and the delay used
#
#   TDS scope trigger off of Low Water V-Dot
#       However, unknown delay of ekspla arrival. Check timing against 
#       Vladamie's code
#

def ekspla(d, optical_delay):

    faux_delay_532 = 19 
    #faux_delay_2color= 19 + 30.
    faux_delay = faux_delay_532

    if optical_delay == 'long':
        optical_delay = 120. # approximate
    elif optical_delay == 'short':
        optical_delay = 12.

    time = np.array(d['Time (sec)'])*10**9 - faux_delay
    data = np.array(d['Ch. 2'])
    snap = np.where(data==data.max())

    return  time[snap[0][0]]

#----------------------------------------------------------------------#
#   Graph two veritcal green lines at the location of the ekspla images
#----------------------------------------------------------------------#
#   assumes there is an existing plot for the current
#   
def graph_ekspla(one, delay):
    ax = plt.gca()
    ax.axvline(one, color='green', label="1S")
    if delay:
        ax.axvline((one + delay), color='green', label="1D")
    plt.xlim(-100,400)
    plt.xlabel("Time (ns)")
    plt.ylabel("Current (?)")
    plt.legend()
    plt.draw()

def get_ekspla(path=None, delay='single'):


    shotnumber, d = cscopes.read_scope_from_directory(path, 'TDS8', 'DIAG')

    stack_bdot = np.array(d['Ch. 3'])
    time = np.array(d['Time (sec)'])*10**9
    t, current = cmath.integrate_signal(time, stack_bdot)
    plt.plot(t, current/current.max(), label='Stack Current')
    #plt.plot(time, stack_bdot/stack_bdot.max(), label='Stack Bdot')

    snap = ekspla(d, delay)
    graph_ekspla(snap, delay)
    plt.title(shotnumber)



if __name__ is "__main__":
    print '''
    #==========================================================================#
    #   EKSPLA Analysis
    #==========================================================================#
    
       running get_ekspla() will prompt you for the Z_SHOT directory
       and append two vertical lines onto an open graph at the time locations
       of the ekspla snapshots
    
       The long delay (120 ns) is default. To change this edit code to:
       get_ekspla(delay='short')
    
    '''
    plt.ion()
    plt.close('all')
    path = cscopes.prompt_for_path()
    get_ekspla(path, delay=None)

