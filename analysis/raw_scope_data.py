import os
import sys
import matplotlib.pyplot as plt

CORE_DIR = os.path.dirname(os.path.abspath(__file__+'/..'))

if CORE_DIR not in sys.path:
    sys.path.append(CORE_DIR)

from core import cplot
from core import cscopes
from core import cmath


def get_scope_data(path, scopename, scopetype, chs=["all"],
                    plot_multi_shots=False):

    shotnumber, raw_data = cscopes.read_scope_from_directory(path, 
                                                         scopename, 
                                                         scopetype)
    
    scope_full_name = scopename + '_' + scopetype

    # run this section for multiple shots and comparing
    if plot_multi_shots:
        assert False
        shot_title = ' '.join((scopename, scopetype))
        graph_multi_shots(shot_title, shotnumber, channels)
    #  run this section for single shot
    else:
        shot_title = ' '.join((shotnumber, scopename, scopetype))
        graph_single_shot(shot_title, raw_data, chs)


def grap_specs(shot_title):
    ax = plt.gca()
    ax.set_title(shot_title)
    ax.set_xlabel('Time (ns)')
    ax.set_xlim(-100,700)
    ax.set_ylabel('Volts')
    ax.legend()
    plt.show()

#----------------------------------------------------------------------#
#   Graph scope from one shot
#----------------------------------------------------------------------#
#
def graph_single_shot(shot_title, raw_data, chs):
    plt.close('all')
    
    print raw_data.keys()
    time = raw_data['Time (sec)'] * 10**9

    for channel_name, channel in raw_data.iteritems():
        if (channel_name in chs) or ('all' in chs) and not channel_name == 'Time (sec)':
            print 'plotting %s' %channel_name
            plt.plot(time[:-500], 
                     channel[:-500], 
                     label=channel_name)

    grap_specs(shot_title)

    
#----------------------------------------------------------------------#
#   Graph multible shots
#----------------------------------------------------------------------#
def graph_multi_shots(shot_title, shotnumber, data):

    for key in data.iterkeys():
        if (key != 'Time (ns)') and ((key in CH) or ('all' in CH)):
            shotlabel = ' '.join((shotnumber, key))
            print 'plotting %s' %shotlabel
            plt.plot(data[key]['Time (ns)'][:-500], 
                     data[key]['Data'][:-500], 
                     label=shotlabel)

    grap_specs(shot_title)

if __name__ is "__main__":
    plt.ion()

    # The scope to plot
    scopename = 'TLS3'
    scopetype = 'DIAG'

    print '''
    #==========================================================================#
    #   View Raw Data on Scope
    #==========================================================================#
    
      No time correction
      Set scope to view in code via: SCOPE_NAME, SCOPE_TYPE global variables

    '''
    path = cscopes.prompt_for_path()
    get_scope_data(
                path=path,
                scopename=scopename,
                scopetype=scopetype,
                chs=["all"],
            )

    
