#==============================================================================#
#   Analyse the XRD and PCD from the 5ch head
#==============================================================================#
#
#   The 5 Ch head is feed onto two scopes TDS 3 and 4, both are triggered off of
#   the stack b dot.
#   This script takes two of the XRDs or PCDs off of each scope, preforms a time
#   correction and plots them

#----------------------------------------------------------------------#
#   Imports
#----------------------------------------------------------------------#
#   check path to see if the core_diag directory is in the system path
#   import the core_diag module for shared functionality between diagnostics
#       i.e. read and write files, graphing, etc

import os
import sys
import matplotlib.pyplot as plt

CORE_DIR = os.path.dirname(os.path.abspath(__file__+'/..'))

if CORE_DIR not in sys.path:
    sys.path.append(CORE_DIR)

from core import cplot
from core import cscopes
from core import cmath


#----------------------------------------------------------------------#
#   Cable delays
#----------------------------------------------------------------------#
#   Currently configured for 4 PCDs on Two scopes, triggered off of stack bdot

timing_file = '/Users/bdhammel/Documents/NTF/Analysis/2014_5_Jet/cable_delays.json'

TDS3 = cscopes.import_scope_preferences('TDS3_DIAG')
TDS4 = cscopes.import_scope_preferences('TDS4_DIAG')
TDS5 = cscopes.import_scope_preferences('TDS5_DIAG')
TOF = 6.6                                   #   2m 5ch head in feet (ns) 
                                            #   from TCC

def stack_bdot_to_current(bdot_voltage):
    """Integrate the stack Bdot signal

    No calibration factor is know for the stack bdot. However, it is useful to 
    compare to for timing purposes

    Returns:
        current profile (dict): constructed in the same format as tds3_data
    """
    voltage = bdot_voltage.signal()
    time = bdot_voltage.time()
    voltage -= voltage[:200].mean() # noise offset
    t, I = cmath.integrate_signal(time, voltage)
    return {'Name':'Current', 'Data':I, 'Time (ns)':t}

def stack_bdot():
    return TDS3.get_channel('Ch. 4')


def graph(pcd_data, stack_data, shotnumber):
    plt.close('all')
    fig = plt.figure('PCDs')
    ax = fig.add_subplot(111)
    ax.plot(stack_data.time(), stack_data.signal(), 'y', label='stack')
    ax.set_ylabel('Current (?)')
    ax.set_xlabel('Time (ns)')
    ax2 = ax.twinx()
    ax2.set_ylabel('Voltage (V)')

    for pcd, trace in pcd_data.iteritems():
        print "plotting " + pcd
        time, signal = trace.data()
        ax2.plot(time, signal, label=pcd)

    ax2.legend()
    ax.set_xlim(-30,250)
    ax2.set_ylim(-1.,15.)
    cplot.align_yaxis()
    plt.title(shotnumber)
    plt.draw()

def get_xrays(path):

    shotnumber, tds3_raw_data = cscopes.read_scope_from_directory(path, 'TDS3', 'DIAG')
    TDS3.load_data(tds3_raw_data)
    shotnumber, tds4_raw_data = cscopes.read_scope_from_directory(path, 'TDS4', 'DIAG')
    TDS4.load_data(tds4_raw_data)
    TDS4.set_trigger_to_external_scope(TDS3)
    shotnumber, tds5_raw_data = cscopes.read_scope_from_directory(path, 'TDS5', 'DIAG')
    TDS5.load_data(tds5_raw_data)
    TDS5.set_trigger_to_external_scope(TDS3)

    tds3_data = TDS3.get_channels() 
    tds4_data = TDS4.get_channels()
    tds5_data = TDS5.get_channels()

    pcd1 = tds3_data['Ch. 1']
    pcd2 = tds3_data['Ch. 2']
    pcd3 = tds3_data['Ch. 3']
    #pcd4 = tds5_data['Ch. 3']
    pcd4 = tds4_data['Ch. 3']

    pcd_data = {pcd1.verbose_name:pcd1,
                pcd2.verbose_name:pcd2,
                pcd3.verbose_name:pcd3,
                pcd4.verbose_name:pcd4}

    stack_data = stack_bdot() 

    graph(pcd_data, stack_data, shotnumber)

if __name__ is "__main__":
    plt.ion()
    print '''
    #==========================================================================#
    #   XRD / PCD Analysis
    #==========================================================================#
    '''
    path = cscopes.prompt_for_path()
    get_xrays(path)



